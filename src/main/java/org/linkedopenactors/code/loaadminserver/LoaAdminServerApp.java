package org.linkedopenactors.code.loaadminserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import de.codecentric.boot.admin.server.config.EnableAdminServer;

@SpringBootApplication
@EnableAdminServer
public class LoaAdminServerApp {

	public static void main(String[] args) {
		SpringApplication.run(LoaAdminServerApp.class, args);
	}

}
